import AppNavBar from './components/AppNavBar';

import Home from './pages/Home';
import Register from './pages/Register';
import Courses from './pages/Courses';
import ErrorPage from './pages/Error';
import LoginPage from './pages/Login';
import CourseView from './pages/CourseView';
import LogoutPage from './pages/Logout';
import AddCourse from './pages/AddCourse';
import UpdateCourse from './pages/UpdateCourse'

import { useState, useEffect } from 'react';

import { UserProvider } from './UserContext';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';


import './App.css';

function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {

    localStorage.clear('accessToken');
    setUser({
      id: null,
      isAdmin: null
    })
  }
  
  useEffect(() => {

    let token = localStorage.getItem('accessToken')
    fetch('https://murmuring-cliffs-95029.herokuapp.com/users/id', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => res.json()).then(data => {

      if (typeof data._id !== 'undefined') {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      } else {

        setUser({
          id: null,
          isAdmin: null
        });
      }
    })
  }, [user]);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        <Routes>         
          <Route path='/' element={<Home />} />
          <Route path='/register' element={<Register/>} /> 
          <Route path='/courses' element={<Courses/>} />
          <Route path='/login' element={<LoginPage/>} />
          <Route path='/logout' element={<LogoutPage/>} />
          <Route path='/courses/view/:id' element={<CourseView/>} />
          <Route path='/courses/add' element={<AddCourse/>} />
          <Route path='/courses/update' element={<UpdateCourse/>} />
          <Route path='/*' element={<ErrorPage/>} />
        </Routes> 
      </Router> 
    </UserProvider>    
  );
}

export default App;

import { Row, Col } from 'react-bootstrap'

export default function AboutMe() {
	return(

		<Row className='text-light bg-dark p-5'>
			<Col className="bg-primary">
				<h1 className="p-5"></h1>
			</Col>

			<Col lg={12} md={12} xs={12}>
				<h2 className="pt-4">About Me</h2>
				<h1 className="pt-4">John Rowell Lebaste</h1>
				<h5 className="pt-1">Full Stack Web Developer</h5>
				<h6 className="pt-1">Licensed Civil Engineer who wants to pursue his passion in coding and become a Full Stack Web Developer</h6>
			</Col>

			<Col lg={12} md={12} xs={12}>
				<h3 className="pt-5">Contacts</h3>
				<ul>
					<li>Email: johnro@email.com</li>
					<li>Mobile No: 09508803601</li>
					<li>Address: Santa Maria, Bulacan</li>
				</ul>
			</Col>
		</Row>
	);
}
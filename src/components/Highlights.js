import {Row, Col, Card, Container} from 'react-bootstrap';

export default function Highlights() {
	return(

		<Container>
			<Row className="my-3">
				<Col sx={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> Learn From Home</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur, adipisicing elit. Optio, nam?	
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col sx={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> Study Now, Pay Later</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur, adipisicing elit. Optio, nam?	
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col sx={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> Be Part of our Community </Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet consectetur, adipisicing elit. Optio, nam?	
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		
		);
};
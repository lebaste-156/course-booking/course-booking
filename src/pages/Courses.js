import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import Hero from '../components/Banner';
import CourseCard from '../components/CourseCard';

const data = {
	title: 'Course Catalog',
	content: 'Browse through our Catalog of Courses'
}
export default function Course() {
	const [coursesCollection, setCoursesCollection] = useState([]);

	useEffect(() => {
		fetch('https://murmuring-cliffs-95029.herokuapp.com/courses/').then(res => res.json()).then(convertedData => {
			setCoursesCollection(convertedData.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course}/>
				)
			}))
		});
	},[]);

	return(
		<>
			<Hero bannerData={data}/>
			<Container>
				{coursesCollection}
			</Container>	
		</>
	);
};
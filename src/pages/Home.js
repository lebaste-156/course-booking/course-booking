import Banner from './../components/Banner';
import Highlights from './../components/Highlights';

const data = {
	title: 'Welcome to the Home Page',
	content: 'Opportunities for everyone, everywhere'
}

export default function Home() {
	return(
		<>
			<Banner bannerData={data}/>
			<Highlights />
		</>
	);
};
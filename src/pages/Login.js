import { useState, useEffect, useContext } from 'react'
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';

const data = {
	title: 'Welcome to Login Page',
	content: 'Sign in your account below',
}
export default function Login() {

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail ]= useState('');
	const [password, setPassword ]= useState('');

	const [ isActive, setIsActive] = useState(false);
	const [ isValid, setIsValid ] = useState(false);

	let addressSign = email.search('@')
	let dns = email.search('.com')

	useEffect(() => {
		if (dns !== -1 && addressSign !== -1) {
			setIsValid(true)
			if (password !== '') {
				setIsActive(true)
			} else {
				setIsActive(false)
			}

		} else {
			setIsValid(false)
			setIsActive(false)
		}
	},[email, password, addressSign, dns])

	const LoginUser = async(event) => {
		event.preventDefault()

		fetch('https://murmuring-cliffs-95029.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => res.json()).then(data => {
		
			let token = data.access

			if (typeof token !== 'undefined') {

				localStorage.setItem('accessToken' ,token)

				fetch('https://murmuring-cliffs-95029.herokuapp.com/users/id', {
				  headers: {
				    Authorization: `Bearer ${token}`
				  }
				}).then(res => res.json()).then(data => {

				  if (typeof data._id !== 'undefined') {

				    setUser({
				      id: data._id,
				      isAdmin: data.isAdmin
				    });
				  } else {

				    setUser({
				      id: null,
				      isAdmin: null
				    });
				  }
				})
				
				Swal.fire({
					icon: 'success',
					title: 'Successfully Login',
					text: 'You are now login',
					timer: 5000
				}).then(() => {
					window.location.href = '/'
				})

			} else {
				Swal.fire({
					icon: 'error',
					title: "Check your credentials",
					text: "Incorrect Email or Password"
				})
			}
		})

	};

	return(
		user.id ?
		<Navigate to='/' />
		:
		<>
			<Hero bannerData={data}/>	
			<Container>
				<h1 className="text-center"> Login Form </h1>
				<Form onSubmit={e => LoginUser(e)}>
					<Form.Group>
						<Form.Label> Email: </Form.Label>
						<Form.Control type='email' placeholder="Enter your Email" value={email} onChange={event => {setEmail(event.target.value)}} required/>
					{
						isValid? 
					<h6 className="text-success">Email is Valid</h6>
						:
					<h6 className="text-danger">Email is Invalid</h6>
					}
					</Form.Group>
					<Form.Group>
						<Form.Label> Password: </Form.Label>
						<Form.Control type='password' placeholder="Enter your Password" value={password} onChange={event => {setPassword(event.target.value)}} required/>
					</Form.Group>

					{
						isActive ?

						<Button type="submit" className="btn-block btn-success">
							Login
						</Button>
						:
						<Button className="btn-secondary btn-block" disabled>
							Login
						</Button>
					}
				</Form>
			</Container>
		</>
	);
};
import { useState, useEffect } from 'react'
import Hero from '../components/Banner';
import {Row, Col, Card, Button, Container} from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import Swal from 'sweetalert2'

const data = {
	title: 'Welcome to b156 Booking App',
	content: 'Check out our school campus'
};

export default function CourseView() {

	const [ courseInfo, setCourseInfo ] = useState({
		name: null,
		description: null,
		price: null
	})

	const {id} = useParams()

	useEffect(() =>{
		fetch(`https://murmuring-cliffs-95029.herokuapp.com/courses/${id}`).then(res => res.json()).then(convertedData => {

			setCourseInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		})
	},[courseInfo, id])

	const enroll = () => {
		return(
			Swal.fire(
			{
				icon: 'success',
				title: 'Enrolled Successfully',
				text: 'Thank you for enrolling to this course'
			})

		);
	};

	return(
		<>
			<Hero bannerData={data} />
			<Row>
				<Col>
					<Container>
						<Card className="text-center">
							<Card.Body>
								<Card.Title>
									<h2> {courseInfo.name} </h2>
								</Card.Title>
								<Card.Subtitle>
									<h6 className="mt-4"> Description: </h6>
								</Card.Subtitle>
								<Card.Text>
									{courseInfo.description}
								</Card.Text>
								<Card.Subtitle>
									<h6 className="mt-4"> PHP: </h6>
								</Card.Subtitle>
								<Card.Text>
									{courseInfo.price}
								</Card.Text>
							</Card.Body>

							<Button variant="warning" className="btn-block" onClick={enroll}>
								Enroll
							</Button>

							<Link className="btn btn-success btn-block mb-5" to="/login">
								Login to Enroll
							</Link>
						</Card>
					</Container>
				</Col>
			</Row>
		</>
	);
};
import Hero from '../components/Banner';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'
import { useState, useEffect, useContext } from 'react'
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';

const data = {
	title: 'Welcome  to the Register Page',
	content: 'Create an account to enroll'
}

export default function Register() {

	const { user } = useContext(UserContext);

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');
	const [ mobileNo, setMobileNo ] = useState('');

	const [ isActive, setIsActive ] = useState(false);
	const [ isMatched, setIsMatched ] = useState(false);
	const [ isMobileValid, setIsMobileValid] = useState(false);
	const [ isAllowed, setIsAllowed] = useState(false);

	useEffect(() => {

		if (password1 === password2 && (password1 && password2 !== '')) {
			setIsMatched(true);

			if (mobileNo.length === 11) {
				setIsMobileValid(true);
				if (firstName && lastName && email !== '') {
					setIsAllowed(true)
					setIsActive(true)
				} else {
					setIsAllowed(false)
					setIsActive(false)
				}
				
			} else {
				setIsMobileValid(false)
				setIsAllowed(false)
				setIsActive(false)
			 }

		} else if (mobileNo.length === 11) {
			setIsMobileValid(true)
		} else {
			setIsActive(false);
			setIsMatched(false);
			setIsMobileValid(false);
			setIsAllowed(false);

		}
	},[firstName, lastName, email, password1, password2, mobileNo]);

	const registerUser = async(eventSubmit) => {
		eventSubmit.preventDefault()

		const isRegistered = await fetch('https://murmuring-cliffs-95029.herokuapp.com/users/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body : JSON.stringify({
			    firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNo: mobileNo
			})
		}).then(response => response.json()).then(data => {
			console.log(data);

			if (data.email) {
				return true
			} else {
				return false
			}
		})

			if(isRegistered) {
				setFirstName('');
				setLastName('');
				setEmail('');
				setPassword1('');
				setPassword2('');
				setMobileNo('');

				Swal.fire({
					icon: 'success',
					title: 'Registration Successful',
					text: 'Thank you for creating an account',
					timer: 5000
				}).then(() => {
					window.location.href = "/login"
				})

			} else {
				Swal.fire({
					icon: 'error',
					title: "Something Went Wrong",
					text: "Try Again Later!"
				})
			}


	}

	return(
		
		user.id ?
			<Navigate to='/' replace={true}/>
		:
		<>
			<Hero bannerData={data}/>
			<Container>
				{
					isAllowed ?
				<h1 className="text-center text-success">You May Now Register!</h1>
					:
				<>
				<h1 className="text-center">Register Form</h1>
				<h6 className="text-center text-secondary"> Fill Up the Form Below</h6>
				</>
				}

				<Form onSubmit={e => registerUser(e)}>
					<Form.Group>
						<Form.Label> First Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter your First Name" value={firstName} onChange={event => {setFirstName(event.target.value)}} required />
					</Form.Group>

					<Form.Group>
						<Form.Label> Last Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter your Last Name" value={lastName} onChange={event => {setLastName(event.target.value)}} required />
					</Form.Group>

					<Form.Group>
						<Form.Label> Email: </Form.Label>
						<Form.Control type="email" placeholder="Enter your Email" value={email} onChange={event => {setEmail(event.target.value)}} required />
					</Form.Group>

					<Form.Group>
						<Form.Label> Password: </Form.Label>
						<Form.Control type="password" placeholder="Enter your Password" value={password1} onChange={event => {setPassword1(event.target.value)}} required />
					</Form.Group>

					<Form.Group>
						<Form.Label> Confirm Password: </Form.Label>
						<Form.Control type="password" placeholder="Confirm your Password" value={password2} onChange={event => {setPassword2(event.target.value)}} required />
						{
							isMatched ?
							<span className="text-success">Passwords Matched!</span>
							:
							<span className="text-danger">Passwords Should Match!</span>
							}
					</Form.Group>

					
					

					<Form.Group>
						<Form.Label> Mobile Number: </Form.Label>
						<Form.Control type="number" placeholder="Enter your Mobile Number" value={mobileNo} onChange={event => {setMobileNo(event.target.value)}} required />
						{
							isMobileValid? 
							<span className="text-success">Mobile Number is Valid</span>
							:
							<span className="text-danger">Mobile Number Should Be 11-digits</span>
						}
					</Form.Group>

					
					

					{
						isActive ? 
							<Button type="submit" variant="success" className="btn-block">
								Register
							</Button>
							:
							<Button variant="secondary" className="btn-block" disabled>
								Register
							</Button>
					}


					
				</Form>		
			</Container>
		
		</>
	);
};
import Header from './../components/Banner';

const data = {
	title: '404 NOT FOUND',
	content: 'Page does not exist'
}

export default function Error() {
	return(
		<Header bannerData={data}/>
	);
};

// export default Error = () => {
// 	return(
// 		<Header bannerData={data}/>
// 	);
// };


import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddCourse() {

	const AddCourse = (event) => {
		event.preventDefault();
		return(
			Swal.fire({
				icon: 'success',
				title: 'Course added',
				text: 'Yeah boi'
			})
		);
	};

	return(
			<Container className="mt-5">
				<Form onSubmit={e => AddCourse(e)}>
					<h1 className="text-center">Add Course</h1>
					<Form.Group>
						<Form.Label>Course Name</Form.Label>
						<Form.Control type="text" placeholder="Enter Course Name " required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Description</Form.Label>
						<Form.Control type="text" placeholder="Enter Course Description" required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Price</Form.Label>
						<Form.Control type="number" placeholder="Enter Course Price" required />
					</Form.Group>
					<Button type="submit">
						Add
					</Button>
				</Form>
				
			</Container>
	);
};